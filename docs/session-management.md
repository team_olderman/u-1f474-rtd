# How to Manage Sessions

Authentication is controlled by following steps.


1. When the request is sent, it returns set-cookie with 'API' key

2. Each session is managed by Redis with given ID as 'api-session:'

3. When user logged in with given 'API' cookie, it finds its session with given id and updates its user information on session

4. With given cookie, client can access to the login required fields
