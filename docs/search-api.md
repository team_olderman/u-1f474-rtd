# Search API

##  Search by name
: /v1/search/(keyword)

Search API gives department, lecture and lecturer information that starts with given keyword. We do escape keyword for preventing SQL injection.

- Examples
```
GET /v1/search/전산
```

```
HTTP 200 OK
Content-Type: application/json

{
    "lecturer": [],
    "lecture": [
        {
            "name_eng": "Computational Physics",
            "code_name": "PH413",
            "dept_id": 110,
            "system_num": 3364455,
            "name_kor": "전산물리학개론",
            "college_id": 2
        },
        {
            "name_eng": "Computational Methods in Structural Analysis",
            "code_name": "CE313",
            "dept_id": 441,
            "system_num": 5369367,
            "name_kor": "전산구조해석",
            "college_id": 12
        },
        {
            "name_eng": "Introduction to Computer Application",
            "code_name": "CC510",
            "dept_id": 973,
            "system_num": 1732788,
            "name_kor": "전산응용개론",
            "college_id": 101
        },
        {
            "name_eng": "Computational Nanoscience: Electronic Structure theory",
            "code_name": "NST551",
            "dept_id": 3692,
            "system_num": 10504261,
            "name_kor": "전산나노과학: 전자구조이론",
            "college_id": 102
        },
        {
            "name_eng": "Computational Nanomaterials Physics",
            "code_name": "NST651",
            "dept_id": 3692,
            "system_num": 10505557,
            "name_kor": "전산 나노재료 물리",
            "college_id": 102
        },
        {
            "name_eng": "Computational Linear Algebra",
            "code_name": "MAE607",
            "dept_id": 4417,
            "system_num": 6726247,
            "name_kor": "전산선형대수",
            "college_id": 6
        },
        {
            "name_eng": "Computer Organization",
            "code_name": "CS311",
            "dept_id": 4421,
            "system_num": 5322709,
            "name_kor": "전산기조직",
            "college_id": 8
        },
        {
            "name_eng": "Introduction to Computer Networks",
            "code_name": "CS341",
            "dept_id": 4421,
            "system_num": 5322817,
            "name_kor": "전산망 개론",
            "college_id": 8
        },
        {
            "name_eng": "Computer Science Project",
            "code_name": "CS408",
            "dept_id": 4421,
            "system_num": 5323976,
            "name_kor": "전산학 프로젝트",
            "college_id": 8
        },
        {
            "name_eng": "Special Topics in Computer Science",
            "code_name": "CS492",
            "dept_id": 4421,
            "system_num": 5324294,
            "name_kor": "전산학특강<디자인과 플랫폼: 모바일 IoT 및 유비쿼터스 컴퓨팅의 새로운 파라다임과 플랫폼>",
            "college_id": 8
        },
        {
            "name_eng": "Special Topics in Computer Science II",
            "code_name": "CS494",
            "dept_id": 4421,
            "system_num": 5324296,
            "name_kor": "전산학특강 II<공개 소프트웨어 소개>",
            "college_id": 8
        }
    ],
    "college": [
        {
            "name_eng": "School of Computing",
            "_id": 4421,
            "name_kor": "전산학부",
            "college_id": 8
        }
    ]
}
```
