# Before we use

Since KAIST database system stores so many information via string, we converted some of the repeated string into enumerated list (integer). So, you need to have this mapping table for understanding our query better.

## Program Specification (used in college-api as a parameter)

```
{'other': 0,
'bachelor': 1,
'master': 3,
'phd': 5,
}
```

## Classification Type (result of lecture-api)

```
{
    '인문사회필수': 11, '교양필수': 12, '교양필수(봉사)': 13, '교양필수(체육)': 14, '인문사회선택': 15, '교양선택': 16,
    '공통필수': 21, '기초필수': 22, '공통기초': 23, '기초선택': 24,
    '전공': 31, '전공필수': 32, '학제간필수': 33, '트랙필수': 34, '전공선택': 35, '학제간': 36, '선택(석/박사)': 37,
    '세미나': 41, '논문세미나': 42, '연구필수': 43, '개별연구': 44, '졸업연구': 45, '논문연구': 46,
    '인턴쉽': 51, '현장실습및연구': 52,
    '자유선택': 61,
    '없음': 71,
}
```

## Target Type (result of lecture-api)

```
{
    '학사과정': 1, '석박사통합과정': 2, '석박사통합과정(박)': 3,
}
```
