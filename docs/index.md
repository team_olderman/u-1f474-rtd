# Senior's Way (SWAY) API server documentation

This documentation is written for the people who want to use API for KAIST academic system.

Since KAIST has been growing so rapidly, the academic system also had many changed to keep pace with its growth. Thus, to the people who want to develop system for improving welfare for KAIST members,
our team U+1F474 (means 'Older Man' from Unicode v6.0) decided to share our trails by sharing APIs that we have been made.

For that reason, we documented

- How to run our server

- What kinds of API we provided for public / private
