# How to build

## Environment Variables
We stored secure or external informations on its environment variables.
Thus, you should set these following variables. (I strongly recommend you to use virtualenvwrapper)

### Required Variables

```
SALT_VALUE(str)     # 'SOME_RANDOM_SALTS'
SECRET_KEY(str)     # 'SOME_RANDOM_SECRET_KEY'
FRONTEND_DOMAIN_NAME(str)
API_PORT(int)   # opened port
PORT(int)       # forwared port
DATABASE_URL(str)
REDIS_URL(str)
MAIL_USERNAME(str)
MAIL_PASSWORD(str)
```

### Recommended Variables

```
WERCKER_MYSQL_URL(str)     # for testing
FREETDS_DRIVER_ADDRESS(str)
KAIST_ACADEMIC_SERVER_ADDR(str)
KAIST_ACADEMIC_SERVER_PORT(str)
KAIST_ACADEMIC_DATABASE_NAME(str)
KAIST_ACADEMIC_DATABASE_ACCOUNT(str)
KAIST_ACADEMIC_DATABASE_PASSWORD(str)
SENTRY_DSN(str)
NEW_RELIC_LICENSE_KEY(str)
BEAKER_SESSION_DATA_DIR(str)
```


## Build Steps
If you push this service to Heroku instance, then you don't need to follow these steps.
All you need to do is setup a redis server and a database system before you deploy.

### Install required services:

```
$ brew install redis
$ # ( run  'sudo redis-server /usr/local/etc/redis.conf' if needed)
$ brew install mariadb
$ # ( run 'mysql.server start' if needed)
$ brew install npm
```


### Install Javascript dependencies
(we are not handling front-end related things nowadays, but using npm for converting all configuration to production mode)

```
$ npm install    # (on root)
```


### Install Python packages

```
$ pip install -r requirements.txt
$ # For Mac only(because of uWSGI): $export CC=gcc && pip install -r requirements.txt
```


### Set necessary environment variables
(see the 'Environment Variables' section)

```
...
...
$ export MAIL_PASSWORD=hello_world
$ export MAIL_USERNAME=tester@gmail.com
$ export PORT=5000
...
...
```


### Run local server

```
$ # default
$ honcho start
$
$ # run local server only
$ python manage.py runserver --host 0.0.0.0
$
$ # run celery worker only
$ python runcelery.py -A app.tasks worker
$
$ # run test cases
$ python tests.py
```
