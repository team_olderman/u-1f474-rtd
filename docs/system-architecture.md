# System Architecture
Our team mainly focused on automated tasks. Thus, almost all of the decision is based on automation.
So, for the people who want to understand our project, please keep in mind for better understanding of ours.


1. Framework
This API server is running on top of Flask project with Python 2.7.8 (nginx -> uwsgi -> Flask)
We didn't select 3.x for the case that pypy support numpy and 2.7.9 because of gevent + uwsgi issue.


2. Storage
We use Redis and MariaDB for storing / reforming our data. Of course, we have been considered many other options(for example, Cassandra), but we decided to build on top of well known systems because of automated tasks; build, test and run. All data is cached from KAIST academic system for reducing the loads.


3. Automated Tasks.
We are using [wercker](https://app.wercker.com) for automated-driven development. It always start running tests once we push our code to our private [bitbucket](https://bitbucket.org/team_olderman) repository. Also, for automated running, we use Heroku style '.buildpacks'.


4. Task Management
We adopted Celery until now, but still in consideration because our computation is so expensive.


5. Monitoring
Until now, we use sentry for error report and newrelic for status monitoring.
Also, we set google analytics for analyzing user behavior for better understanding of them.
