# Private API list

We currently provide APIs about following things.
- Login / Logout / Sign out
- add / get / delete favorited courses and colleges
- get list of courses that a user is already taken before
- get recommended courses for a user

Please contact to our team if you really need.
