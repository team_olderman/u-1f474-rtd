# Lecture API

##  Get information about a lecture
: /v1/lecture/(college_id)/(code_name)      # or (system_num)

This api returns general information about a course that operated in given college

- Examples
```
GET /v1/college/8/CS408  # ( or /v1/college/8/5323976 )
```

```
HTTP 200 OK
Content-Type: application/json

{
    "lectures": [
        {
            "system_num": 5323976,
            "name_kor": "전산학 프로젝트",
            "name_eng": "Computer Science Project",
            "system_num_str": "36.408",
            "code_name": "CS408",
            "dept_id": 4421,
            "alias_name": null
        }
    ],
    "lecturers": [
        {
            "name_eng": "NameOfTheProfessor",
            "_id": 9016324451455,
            "name_kor": "교수님성함"
        },
        {
            "name_eng": "NameOfTheProfessor2",
            "_id": 1127201050346,
            "name_kor": "교수님성함2"
        }
    ],
    "departments": [
        {
            "name_eng": "School of Computing",
            "_id": 4421,
            "name_kor": "전산학부"
        }
    ],
    "recent_lecture_history": [
        {
            "dept_id": 4421,
            "semester": 3,
            "system_num_str": "36.408",
            "section_str": "A",
            "year": 2015
        },
        {
            "dept_id": 4421,
            "semester": 1,
            "system_num_str": "36.408",
            "section_str": "A",
            "year": 2015
        },
        {
            "dept_id": 3847,
            "semester": 3,
            "system_num_str": "36.408",
            "section_str": "A",
            "year": 2014
        },
        {
            "dept_id": 3847,
            "semester": 1,
            "system_num_str": "36.408",
            "section_str": "A",
            "year": 2014
        },
        {
            "dept_id": 3847,
            "semester": 3,
            "system_num_str": "36.408",
            "section_str": "A",
            "year": 2013
        }
    ],
    "prob_same": [
        {
            "probability": 0.14553,
            "alias_name": null,
            "name_kor": "졸업연구",
            "dept_id": 4421,
            "code_name": "CS490",
            "name_eng": "Research in Computer Science",
            "system_num": 5324292,
            "college_id": 8
        },
        {
            "probability": 0.118503,
            "alias_name": null,
            "name_kor": "운영체제 및 실험",
            "dept_id": 4421,
            "code_name": "CS330",
            "name_eng": "Operating Systems and Lab",
            "system_num": 5322780,
            "college_id": 8
        },
        {
            "probability": 0.10395,
            "alias_name": null,
            "name_kor": "프로그래밍언어",
            "dept_id": 4421,
            "code_name": "CS320",
            "name_eng": "Programming Language",
            "system_num": 5322744,
            "college_id": 8
        },
        {
            "probability": 0.0956341,
            "alias_name": null,
            "name_kor": "데이타베이스 개론",
            "dept_id": 4421,
            "code_name": "CS360",
            "name_eng": "Introduction to Database",
            "system_num": 5322888,
            "college_id": 8
        },
        {
            "probability": 0.0893971,
            "alias_name": null,
            "name_kor": "건강관리<풋살>",
            "dept_id": 3894,
            "code_name": "HSS044",
            "name_eng": "Health Administration",
            "system_num": 1681204,
            "college_id": 1
        }
    ],
    "nth_semester_count": {
        "1": 74,
        "2": 30,
        "3": 28,
        "4": 27,
        "5": 38,
        "6": 47,
        "7": 102,
        "8": 120,
        "9": 42,
        "10": 17,
        "11": 4
    },
    "prob_earlier": [
        {
            "probability": 0.49896,
            "alias_name": null,
            "name_kor": "운영체제 및 실험",
            "dept_id": 4421,
            "code_name": "CS330",
            "name_eng": "Operating Systems and Lab",
            "system_num": 5322780,
            "college_id": 8
        },
        {
            "probability": 0.417879,
            "alias_name": null,
            "name_kor": "프로그래밍언어",
            "dept_id": 4421,
            "code_name": "CS320",
            "name_eng": "Programming Language",
            "system_num": 5322744,
            "college_id": 8
        },
        {
            "probability": 0.372141,
            "alias_name": null,
            "name_kor": "전산기조직",
            "dept_id": 4421,
            "code_name": "CS311",
            "name_eng": "Computer Organization",
            "system_num": 5322709,
            "college_id": 8
        },
        {
            "probability": 0.357588,
            "alias_name": null,
            "name_kor": "알고리즘 개론",
            "dept_id": 4421,
            "code_name": "CS300",
            "name_eng": "Introduction to Algorithms",
            "system_num": 5322672,
            "college_id": 8
        },
        {
            "probability": 0.355509,
            "alias_name": null,
            "name_kor": "데이타베이스 개론",
            "dept_id": 4421,
            "code_name": "CS360",
            "name_eng": "Introduction to Database",
            "system_num": 5322888,
            "college_id": 8
        }
    ]
}
```

## Get lectures by a professor
: /v1/lecture/professor/(lecturer_id)/(college_id)/(system_num)
This API returns a list of lectures that created by given professor.

- Examples
```
GET /v1/lecture/professor/9016324451455/8/5323976
```

```
HTTP 200 OK
Content-Type: application/json

[
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2013,
        "dept_id": 3847,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": 9016324451455,
        "grading": false,
        "eng_only": true,
        "section": 11,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "Spring2013_CS408_syllabus.doc",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "B",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    },
    {
        "lecture_about": "",
        "memo": "Assistant Name:.",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2012,
        "dept_id": 3847,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": 9016324451455,
        "grading": false,
        "eng_only": true,
        "section": 12,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "C",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    },
    {
        "lecture_about": "DETAILS",
        "memo": "Assistant Name: TBD",
        "classification_type": 32,
        "lab": 6,
        "semester": 3,
        "au": 0,
        "year": 2011,
        "dept_id": 3847,
        "system_num": 5323976,
        "evaluation_criteria": "DETAILS",
        "may_have_teamproj": null,
        "_id": 9016324451455,
        "grading": false,
        "eng_only": true,
        "section": 0,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": "http://www.google.com"
    },
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2011,
        "dept_id": 3847,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": 9016324451455,
        "grading": false,
        "eng_only": true,
        "section": 11,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "CS408 syllabus.doc",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "B",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": "http://www.google.com"
    },
    {
        "lecture_about": "DETAILS",
        "memo": "Assistant Name:",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2008,
        "dept_id": 532,
        "system_num": 5323976,
        "evaluation_criteria": "DETAILS",
        "may_have_teamproj": null,
        "_id": 9016324451455,
        "grading": false,
        "eng_only": false,
        "section": 0,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": "http://www.google.com"
    }
]
```

## Get lectures by semester
: /v1/lecture/(college_id)/(system_num)/(year)/(semester)
This API returns a list of lectures that created in given semester

- Examples
```
GET /v1/lecture/8/5323976/2015/1
```

```
HTTP 200 OK
Content-Type: application/json

[
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2015,
        "dept_id": 4421,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": -10081453233349,
        "grading": true,
        "eng_only": true,
        "section": 10,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "Spring2015_CS408_syllabus_20141113.docx",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "A",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    },
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2015,
        "dept_id": 4421,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": 6936173137407,
        "grading": true,
        "eng_only": true,
        "section": 11,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "Spring2015_CS408_syllabus_20141113.docx",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "B",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    },
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2015,
        "dept_id": 4421,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": -9662706278307,
        "grading": true,
        "eng_only": true,
        "section": 12,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "Spring2015_CS408_syllabus_20141113.docx",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "C",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    },
    {
        "lecture_about": "",
        "memo": " ",
        "classification_type": 32,
        "lab": 6,
        "semester": 1,
        "au": 0,
        "year": 2015,
        "dept_id": 4421,
        "system_num": 5323976,
        "evaluation_criteria": "",
        "may_have_teamproj": null,
        "_id": -5298183315336,
        "grading": true,
        "eng_only": true,
        "section": 13,
        "name_kor": "교수님성함",
        "target_type": 3,
        "syllabus_name": "Spring2015_CS408_syllabus_20141113.docx",
        "credit": 3.0,
        "system_num_str": "36.408",
        "section_str": "D",
        "name_eng": "NameOfTheProfessor",
        "lecture": 1,
        "may_have_presentation": null,
        "syllabus_url": ""
    }
]
```
