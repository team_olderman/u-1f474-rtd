# College API

##  Get list of college & its prorgram
: /v1/college/list/(entrance_year)/(degree_level)

College API is basically developed for getting adequate program per each department. Since each student's program is determined by their own degree level and entrance year, we receive those two information as parameters.

- Examples
```
GET /v1/college/list/2015/1
```

```
HTTP 200 OK
Content-Type: application/json

{
    "1": {
        "url": "http://hss.kaist.ac.kr",
        "category_kor": "인문사회융합과학대학",
        "name_eng": "School of Humanities & Social Sciences",
        "category_eng": "College of Liberal Arts and Convergence Science",
        "_id": 1,
        "name_kor": "인문사회과학부",
        "program_id": 1
    },
    "2": {
        "url": "http://physics.kaist.ac.kr",
        "category_kor": "자연과학대학",
        "name_eng": "Physics",
        "category_eng": "College of Natural Sciences",
        "_id": 2,
        "name_kor": "물리학과",
        "program_id": 2
    },
    "3": {
        "url": "http://mathsci.kaist.ac.kr",
        "category_kor": "자연과학대학",
        "name_eng": "Department of Mathematical Sciences",
        "category_eng": "College of Natural Sciences",
        "_id": 3,
        "name_kor": "수리과학과",
        "program_id": 3
    },
    "4": {
        "url": "http://chem.kaist.ac.kr",
        "category_kor": "자연과학대학",
        "name_eng": "Chemistry",
        "category_eng": "College of Natural Sciences",
        "_id": 4,
        "name_kor": "화학과",
        "program_id": 4
    },
    "5": {
        "url": "http://bio.kaist.ac.kr",
        "category_kor": "생명과학기술대학",
        "name_eng": "Biological Sciences",
        "category_eng": "College of Life Science and Bioengineering",
        "_id": 5,
        "name_kor": "생명과학과",
        "program_id": 5
    },
    "6": {
        "url": "http://me.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Departmemt of Mechanical Engineering",
        "category_eng": "College of Engineering",
        "_id": 6,
        "name_kor": "기계공학과",
        "program_id": 6
    },
    "7": {
        "url": "http://ae.kaist.ac.kr/",
        "category_kor": "공과대학",
        "name_eng": "Departmemt of Aerospace Engineering",
        "category_eng": "College of Engineering",
        "_id": 7,
        "name_kor": "항공우주공학과",
        "program_id": 7
    },
    "8": {
        "url": "https://cs.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "School of Computing",
        "category_eng": "College of Engineering",
        "_id": 8,
        "name_kor": "전산학부",
        "program_id": 8
    },
    "9": {
        "url": "https://www.ee.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "School of Electrical Engineering",
        "category_eng": "College of Engineering",
        "_id": 9,
        "name_kor": "전기및전자공학부",
        "program_id": 9
    },
    "10": {
        "url": "http://bioeng.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Bio and Brain Engineering",
        "category_eng": "College of Engineering",
        "_id": 10,
        "name_kor": "바이오및뇌공학과",
        "program_id": 10
    },
    "11": {
        "url": "https://cbe.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Chemical and Biomolecular Engineering",
        "category_eng": "College of Engineering",
        "_id": 11,
        "name_kor": "생명화학공학과",
        "program_id": 11
    },
    "12": {
        "url": "http://civil.kaist.ac.kr/",
        "category_kor": "공과대학",
        "name_eng": "Civil and Environmental Engineering",
        "category_eng": "College of Engineering",
        "_id": 12,
        "name_kor": "건설및환경공학과",
        "program_id": 12
    },
    "13": {
        "url": "http://id.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Industrial Design",
        "category_eng": "College of Engineering",
        "_id": 13,
        "name_kor": "산업디자인학과",
        "program_id": 13
    },
    "14": {
        "url": "http://ie.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Department of Industrial Systems Engineering",
        "category_eng": "College of Engineering",
        "_id": 14,
        "name_kor": "산업및시스템공학과",
        "program_id": 14
    },
    "15": {
        "url": "http://nuclear.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Nuclear and Quantum Engineering",
        "category_eng": "College of Engineering",
        "_id": 15,
        "name_kor": "원자력및양자공학과",
        "program_id": 15
    },
    "16": {
        "url": "http://mse.kaist.ac.kr",
        "category_kor": "공과대학",
        "name_eng": "Materials Science and Engineering",
        "category_eng": "College of Engineering",
        "_id": 16,
        "name_kor": "신소재공학과",
        "program_id": 16
    },
    "17": {
        "url": "http://btm.kaist.ac.kr/",
        "category_kor": "인문사회융합과학대학",
        "name_eng": "Department of Business and Technology Management",
        "category_eng": "College of Liberal Arts and Convergence Science",
        "_id": 17,
        "name_kor": "기술경영학과",
        "program_id": 17
    }
}
```
