# TechTree API

##  Get information needed for drawing a tech-tree
: /v1/techtree/(program_id)

TechTree API gives information that needed for drawing coursework graph of a program.
It gives depth and relative_position information for drawing purpose.
Each node has recent_year if that class is opened within 2 years.

- Examples
```
GET /v1/tecthree/8
```

```
HTTP 200 OK
Content-Type: application/json

{
    "node": [
        {
            "recent_year": 2015,
            "name_kor": "프로그래밍기초",
            "relative_position": 33,
            "depth": 10,
            "code_name": "CS101",
            "name_eng": "Introduction to Programming",
            "system_num": 5320081,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "프로그래밍 실습",
            "relative_position": 66,
            "depth": 10,
            "code_name": "CS109",
            "name_eng": "Programming Practice",
            "system_num": 5320089,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "디지탈시스템 및 실험",
            "relative_position": 12,
            "depth": 20,
            "code_name": "CS211",
            "name_eng": "Digital System and Lab",
            "system_num": 5321413,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "이산구조",
            "relative_position": 25,
            "depth": 20,
            "code_name": "CS204",
            "name_eng": "Discrete Mathematics",
            "system_num": 5321380,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "지능 로봇 설계 및 프로그래밍",
            "relative_position": 37,
            "depth": 21,
            "code_name": "CS270",
            "name_eng": "Intelligent robot design and programming",
            "system_num": 5321628,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "프로그래밍의 이해",
            "relative_position": 50,
            "depth": 21,
            "code_name": "CS220",
            "name_eng": "Programming Principles",
            "system_num": 5321448,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "데이타구조",
            "relative_position": 62,
            "depth": 20,
            "code_name": "CS206",
            "name_eng": "Data Structure",
            "system_num": 5321382,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "시스템프로그래밍",
            "relative_position": 75,
            "depth": 21,
            "code_name": "CS230",
            "name_eng": "System Programming",
            "system_num": 5321484,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "문제해결기법",
            "relative_position": 87,
            "depth": 21,
            "code_name": "CS202",
            "name_eng": "Problem Solving",
            "system_num": 5321378,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "소프트웨어 공학 개론",
            "relative_position": 7,
            "depth": 30,
            "code_name": "CS350",
            "name_eng": "Introduction to Software Engineering",
            "system_num": 5322852,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "형식언어및오토마타",
            "relative_position": 15,
            "depth": 30,
            "code_name": "CS322",
            "name_eng": "Formal Languages and Automata",
            "system_num": 5322746,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "프로그래밍언어",
            "relative_position": 23,
            "depth": 31,
            "code_name": "CS320",
            "name_eng": "Programming Language",
            "system_num": 5322744,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "데이타베이스 개론",
            "relative_position": 30,
            "depth": 30,
            "code_name": "CS360",
            "name_eng": "Introduction to Database",
            "system_num": 5322888,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "전산망 개론",
            "relative_position": 38,
            "depth": 30,
            "code_name": "CS341",
            "name_eng": "Introduction to Computer Networks",
            "system_num": 5322817,
            "college_id": 8
        },
        {
            "recent_year": 2013,
            "name_kor": "네트워크 과학의 융합적 접근",
            "relative_position": 46,
            "depth": 31,
            "code_name": "CS340",
            "name_eng": "Interdisciplinary Approach to Network Science",
            "system_num": 5322816,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "내장형 컴퓨터 시스템",
            "relative_position": 53,
            "depth": 30,
            "code_name": "CS310",
            "name_eng": "Embedded Computer Systems",
            "system_num": 5322708,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "전산기조직",
            "relative_position": 61,
            "depth": 31,
            "code_name": "CS311",
            "name_eng": "Computer Organization",
            "system_num": 5322709,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "운영체제 및 실험",
            "relative_position": 69,
            "depth": 32,
            "code_name": "CS330",
            "name_eng": "Operating Systems and Lab",
            "system_num": 5322780,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "알고리즘 개론",
            "relative_position": 76,
            "depth": 31,
            "code_name": "CS300",
            "name_eng": "Introduction to Algorithms",
            "system_num": 5322672,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "컴퓨터 그래픽스 개론",
            "relative_position": 84,
            "depth": 32,
            "code_name": "CS380",
            "name_eng": "Introduction to Computer Graphics",
            "system_num": 5322960,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "파이썬을 통한 자연언어처리",
            "relative_position": 92,
            "depth": 32,
            "code_name": "CS372",
            "name_eng": "Natural Language Processing with Python",
            "system_num": 5322926,
            "college_id": 8
        },
        {
            "name_kor": "심볼릭 프로그래밍",
            "depth": -30,
            "code_name": "CS370",
            "name_eng": "Symbolic Programming",
            "system_num": 5322924,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "전산학 프로젝트",
            "relative_position": 11,
            "depth": 40,
            "code_name": "CS408",
            "name_eng": "Computer Science Project",
            "system_num": 5323976,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "모바일 컴퓨팅과 응용",
            "relative_position": 22,
            "depth": 40,
            "code_name": "CS442",
            "name_eng": "Mobile Computing and Applications",
            "system_num": 5324114,
            "college_id": 8
        },
        {
            "name_kor": "VLSI설계개론",
            "relative_position": 33,
            "depth": 40,
            "code_name": "CS410",
            "name_eng": "Introduction to VLSI Design",
            "system_num": 5324004,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "인공지능개론",
            "relative_position": 44,
            "depth": 40,
            "code_name": "CS470",
            "name_eng": "Introduction to Artificial Intelligence",
            "system_num": 5324220,
            "college_id": 8
        },
        {
            "recent_year": 2014,
            "name_kor": "계산이론",
            "relative_position": 55,
            "depth": 40,
            "code_name": "CS422",
            "name_eng": "Computation Theory",
            "system_num": 5324042,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "대화형 컴퓨터그래픽스",
            "relative_position": 66,
            "depth": 40,
            "code_name": "CS482",
            "name_eng": "Interactive Computer Graphics",
            "system_num": 5324258,
            "college_id": 8
        },
        {
            "recent_year": 2013,
            "name_kor": "텍스트마이닝",
            "relative_position": 77,
            "depth": 41,
            "code_name": "CS474",
            "name_eng": "Text Mining",
            "system_num": 5324224,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "컴파일러설계",
            "relative_position": 88,
            "depth": 40,
            "code_name": "CS420",
            "name_eng": "Compiler Design",
            "system_num": 5324040,
            "college_id": 8
        },
        {
            "name_kor": "소프트웨어프로젝트",
            "depth": -40,
            "code_name": "CS455",
            "name_eng": "Software Project",
            "system_num": 5324153,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "비즈니스 프로세스 설계 및 관리",
            "depth": -40,
            "code_name": "CS452",
            "name_eng": "Business Process Engineering and Management",
            "system_num": 5324150,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "컴퓨터윤리와사회문제",
            "depth": -40,
            "code_name": "CS489",
            "name_eng": "Computer Ethics & Social issues",
            "system_num": 5324265,
            "college_id": 8
        },
        {
            "recent_year": 2014,
            "name_kor": "바이오의료응용 집단지성",
            "depth": -40,
            "code_name": "CS476",
            "name_eng": "Collective Intelligence in Biomedical Applications",
            "system_num": 5324226,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "개별연구",
            "depth": -40,
            "code_name": "CS495",
            "name_eng": "Individual Study",
            "system_num": 5324297,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "웹 기술과 경영 전략",
            "depth": -40,
            "code_name": "CS459",
            "name_eng": "Web Technologies and Business Strategies",
            "system_num": 5324157,
            "college_id": 8
        },
        {
            "name_kor": "모바일 응용 개발",
            "depth": -40,
            "code_name": "CS446",
            "name_eng": "Mobile Applications Development",
            "system_num": 5324118,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "전산학특강<자연언어처리의 핵심과 응용>",
            "depth": -40,
            "code_name": "CS492",
            "name_eng": "Special Topics in Computer Science",
            "system_num": 5324294,
            "college_id": 8
        },
        {
            "recent_year": 2014,
            "name_kor": "분산 알고리즘 및 시스템",
            "depth": -40,
            "code_name": "CS443",
            "name_eng": "Distributed Algorithms and Systems",
            "system_num": 5324115,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "소프트웨어 테스팅 자동화 기법",
            "depth": -40,
            "code_name": "CS453",
            "name_eng": "Automated Software Testing",
            "system_num": 5324151,
            "college_id": 8
        },
        {
            "name_kor": "컴퓨터그래픽스개론",
            "depth": -40,
            "code_name": "CS480",
            "name_eng": "Introduction to Computer Graphics",
            "system_num": 5324256,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "인간-컴퓨터 상호작용",
            "depth": -40,
            "code_name": "CS472",
            "name_eng": "Human-Computer Interaction",
            "system_num": 5324222,
            "college_id": 8
        },
        {
            "name_kor": "세미나",
            "depth": -40,
            "code_name": "CS496",
            "name_eng": "Seminar",
            "system_num": 5324298,
            "college_id": 8
        },
        {
            "recent_year": 2012,
            "name_kor": "전산학특강 I<고급알고리즘>",
            "depth": -40,
            "code_name": "CS493",
            "name_eng": "Special Topics in Computer Science I",
            "system_num": 5324295,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "웹기반 소프트웨어 개발",
            "depth": -40,
            "code_name": "CS457",
            "name_eng": "Web-based Software Development",
            "system_num": 5324155,
            "college_id": 8
        },
        {
            "recent_year": 2012,
            "name_kor": "영상처리 입문",
            "depth": -40,
            "code_name": "CS484",
            "name_eng": "Introduction to Image Processing",
            "system_num": 5324260,
            "college_id": 8
        },
        {
            "recent_year": 2013,
            "name_kor": "전산논리학개론",
            "depth": -40,
            "code_name": "CS402",
            "name_eng": "Introduction to Logic for Computer Science",
            "system_num": 5323970,
            "college_id": 8
        },
        {
            "name_kor": "전산망개론",
            "depth": -40,
            "code_name": "CS441",
            "name_eng": "Introduction to Computer Network",
            "system_num": 5324113,
            "college_id": 8
        },
        {
            "recent_year": 2014,
            "name_kor": "IT 서비스공학",
            "depth": -40,
            "code_name": "CS451",
            "name_eng": "IT Service Engineering",
            "system_num": 5324149,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "전산학특강 II<공개 소프트웨어 소개>",
            "depth": -40,
            "code_name": "CS494",
            "name_eng": "Special Topics in Computer Science II",
            "system_num": 5324296,
            "college_id": 8
        },
        {
            "name_kor": "금융 데이터 분석기법",
            "depth": -40,
            "code_name": "CS478",
            "name_eng": "Financial Data Analysis and Mining",
            "system_num": 5324228,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "졸업연구",
            "depth": -40,
            "code_name": "CS490",
            "name_eng": "Research in Computer Science",
            "system_num": 5324292,
            "college_id": 8
        },
        {
            "recent_year": 2015,
            "name_kor": "정보보호개론",
            "depth": -40,
            "code_name": "CS448",
            "name_eng": "Introduction to Information Security",
            "system_num": 5324120,
            "college_id": 8
        }
    ],
    "edge": [
        {
            "target_name": "CS202",
            "source": 0,
            "source_name": "CS101",
            "target": 8,
            "level": 70
        },
        {
            "target_name": "CS206",
            "source": 0,
            "source_name": "CS101",
            "target": 6,
            "level": 70
        },
        {
            "target_name": "CS206",
            "source": 1,
            "source_name": "CS109",
            "target": 6,
            "level": 10
        },
        {
            "target_name": "CS300",
            "source": 8,
            "source_name": "CS202",
            "target": 18,
            "level": 25
        },
        {
            "target_name": "CS220",
            "source": 3,
            "source_name": "CS204",
            "target": 5,
            "level": 25
        },
        {
            "target_name": "CS270",
            "source": 3,
            "source_name": "CS204",
            "target": 4,
            "level": 25
        },
        {
            "target_name": "CS300",
            "source": 3,
            "source_name": "CS204",
            "target": 18,
            "level": 70
        },
        {
            "target_name": "CS322",
            "source": 3,
            "source_name": "CS204",
            "target": 10,
            "level": 50
        },
        {
            "target_name": "CS202",
            "source": 6,
            "source_name": "CS206",
            "target": 8,
            "level": 70
        },
        {
            "target_name": "CS230",
            "source": 6,
            "source_name": "CS206",
            "target": 7,
            "level": 50
        },
        {
            "target_name": "CS300",
            "source": 6,
            "source_name": "CS206",
            "target": 18,
            "level": 75
        },
        {
            "target_name": "CS311",
            "source": 6,
            "source_name": "CS206",
            "target": 16,
            "level": 50
        },
        {
            "target_name": "CS360",
            "source": 6,
            "source_name": "CS206",
            "target": 12,
            "level": 50
        },
        {
            "target_name": "CS380",
            "source": 6,
            "source_name": "CS206",
            "target": 19,
            "level": 50
        },
        {
            "target_name": "CS230",
            "source": 2,
            "source_name": "CS211",
            "target": 7,
            "level": 25
        },
        {
            "target_name": "CS310",
            "source": 2,
            "source_name": "CS211",
            "target": 15,
            "level": 15
        },
        {
            "target_name": "CS320",
            "source": 5,
            "source_name": "CS220",
            "target": 11,
            "level": 15
        },
        {
            "target_name": "CS310",
            "source": 7,
            "source_name": "CS230",
            "target": 15,
            "level": 10
        },
        {
            "target_name": "CS311",
            "source": 7,
            "source_name": "CS230",
            "target": 16,
            "level": 25
        },
        {
            "target_name": "CS330",
            "source": 7,
            "source_name": "CS230",
            "target": 17,
            "level": 75
        },
        {
            "target_name": "CS341",
            "source": 7,
            "source_name": "CS230",
            "target": 13,
            "level": 55
        },
        {
            "target_name": "CS372",
            "source": 18,
            "source_name": "CS300",
            "target": 20,
            "level": 25
        },
        {
            "target_name": "CS380",
            "source": 18,
            "source_name": "CS300",
            "target": 19,
            "level": 50
        },
        {
            "target_name": "CS422",
            "source": 18,
            "source_name": "CS300",
            "target": 26,
            "level": 40
        },
        {
            "target_name": "CS470",
            "source": 18,
            "source_name": "CS300",
            "target": 25,
            "level": 25
        },
        {
            "target_name": "CS311",
            "source": 15,
            "source_name": "CS310",
            "target": 16,
            "level": 10
        },
        {
            "target_name": "CS330",
            "source": 16,
            "source_name": "CS311",
            "target": 17,
            "level": 25
        },
        {
            "target_name": "CS410",
            "source": 16,
            "source_name": "CS311",
            "target": 24,
            "level": 25
        },
        {
            "target_name": "CS420",
            "source": 11,
            "source_name": "CS320",
            "target": 29,
            "level": 65
        },
        {
            "target_name": "CS300",
            "source": 10,
            "source_name": "CS322",
            "target": 18,
            "level": 25
        },
        {
            "target_name": "CS320",
            "source": 10,
            "source_name": "CS322",
            "target": 11,
            "level": 25
        },
        {
            "target_name": "CS372",
            "source": 10,
            "source_name": "CS322",
            "target": 20,
            "level": 15
        },
        {
            "target_name": "CS422",
            "source": 10,
            "source_name": "CS322",
            "target": 26,
            "level": 70
        },
        {
            "target_name": "CS470",
            "source": 10,
            "source_name": "CS322",
            "target": 25,
            "level": 25
        },
        {
            "target_name": "CS340",
            "source": 13,
            "source_name": "CS341",
            "target": 14,
            "level": 10
        },
        {
            "target_name": "CS442",
            "source": 13,
            "source_name": "CS341",
            "target": 23,
            "level": 10
        },
        {
            "target_name": "CS408",
            "source": 9,
            "source_name": "CS350",
            "target": 22,
            "level": 5
        },
        {
            "target_name": "CS420",
            "source": 20,
            "source_name": "CS372",
            "target": 29,
            "level": 10
        },
        {
            "target_name": "CS474",
            "source": 20,
            "source_name": "CS372",
            "target": 28,
            "level": 25
        },
        {
            "target_name": "CS482",
            "source": 19,
            "source_name": "CS380",
            "target": 27,
            "level": 25
        },
        {
            "target_name": "CS474",
            "source": 25,
            "source_name": "CS470",
            "target": 28,
            "level": 10
        }
    ],
    "group": []
}
```
